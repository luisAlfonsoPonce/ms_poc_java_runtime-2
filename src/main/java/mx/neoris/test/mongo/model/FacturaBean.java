package mx.neoris.test.mongo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * Clase bean para factura
 *
 * @author neoris
 * @version 1.0.0
 */
@Document(collection = "rpt_facturas")
public class FacturaBean implements Serializable {
    /**
     * Serializador.
     */
    private static final long serialVersionUID = 4131271366186327064L;

    @Id
    private ObjectId id;

    private String ide_id;
    private String cve_fol_interno;
    private String cve_fol_sat;
    private String tpo_documento;
    private String bnd_estatus;
    private String txt_rfc_emisor;
    private String txt_rfc_receptor;
    private String fec_cancelar;
    private String fec_emision;
    private String mto_subtotal;
    private String imp_iva;
    private String mto_total;
    /**
     * @return the id
     */
    public ObjectId getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(ObjectId id) {
        this.id = id;
    }
    /**
     * @return the ide_id
     */
    public String getIde_id() {
        return ide_id;
    }
    /**
     * @param ide_id the ide_id to set
     */
    public void setIde_id(String ide_id) {
        this.ide_id = ide_id;
    }
    /**
     * @return the cve_fol_interno
     */
    public String getCve_fol_interno() {
        return cve_fol_interno;
    }
    /**
     * @param cve_fol_interno the cve_fol_interno to set
     */
    public void setCve_fol_interno(String cve_fol_interno) {
        this.cve_fol_interno = cve_fol_interno;
    }
    /**
     * @return the cve_fol_sat
     */
    public String getCve_fol_sat() {
        return cve_fol_sat;
    }
    /**
     * @param cve_fol_sat the cve_fol_sat to set
     */
    public void setCve_fol_sat(String cve_fol_sat) {
        this.cve_fol_sat = cve_fol_sat;
    }
    /**
     * @return the tpo_documento
     */
    public String getTpo_documento() {
        return tpo_documento;
    }
    /**
     * @param tpo_documento the tpo_documento to set
     */
    public void setTpo_documento(String tpo_documento) {
        this.tpo_documento = tpo_documento;
    }
    /**
     * @return the bnd_estatus
     */
    public String getBnd_estatus() {
        return bnd_estatus;
    }
    /**
     * @param bnd_estatus the bnd_estatus to set
     */
    public void setBnd_estatus(String bnd_estatus) {
        this.bnd_estatus = bnd_estatus;
    }
    /**
     * @return the txt_rfc_emisor
     */
    public String getTxt_rfc_emisor() {
        return txt_rfc_emisor;
    }
    /**
     * @param txt_rfc_emisor the txt_rfc_emisor to set
     */
    public void setTxt_rfc_emisor(String txt_rfc_emisor) {
        this.txt_rfc_emisor = txt_rfc_emisor;
    }
    /**
     * @return the txt_rfc_receptor
     */
    public String getTxt_rfc_receptor() {
        return txt_rfc_receptor;
    }
    /**
     * @param txt_rfc_receptor the txt_rfc_receptor to set
     */
    public void setTxt_rfc_receptor(String txt_rfc_receptor) {
        this.txt_rfc_receptor = txt_rfc_receptor;
    }
    /**
     * @return the fec_cancelar
     */
    public String getFec_cancelar() {
        return fec_cancelar;
    }
    /**
     * @param fec_cancelar the fec_cancelar to set
     */
    public void setFec_cancelar(String fec_cancelar) {
        this.fec_cancelar = fec_cancelar;
    }
    /**
     * @return the fec_emision
     */
    public String getFec_emision() {
        return fec_emision;
    }
    /**
     * @param fec_emision the fec_emision to set
     */
    public void setFec_emision(String fec_emision) {
        this.fec_emision = fec_emision;
    }
    /**
     * @return the mto_subtotal
     */
    public String getMto_subtotal() {
        return mto_subtotal;
    }
    /**
     * @param mto_subtotal the mto_subtotal to set
     */
    public void setMto_subtotal(String mto_subtotal) {
        this.mto_subtotal = mto_subtotal;
    }
    /**
     * @return the imp_iva
     */
    public String getImp_iva() {
        return imp_iva;
    }
    /**
     * @param imp_iva the imp_iva to set
     */
    public void setImp_iva(String imp_iva) {
        this.imp_iva = imp_iva;
    }
    /**
     * @return the mto_total
     */
    public String getMto_total() {
        return mto_total;
    }
    /**
     * @param mto_total the mto_total to set
     */
    public void setMto_total(String mto_total) {
        this.mto_total = mto_total;
    }
}

