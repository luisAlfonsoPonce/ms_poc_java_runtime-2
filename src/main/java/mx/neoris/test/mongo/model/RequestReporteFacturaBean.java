package mx.neoris.test.mongo.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase bean para solicitud
 * de reporte de factura
 *
 * @author neoris
 * @version 1.0.0
 */
public class RequestReporteFacturaBean {

    //Contenido paginado
    private List<FacturaBean> content;

    /**
     * @return the content
     */
    public List<FacturaBean> getContent() {
        List<FacturaBean> contenttmp = new ArrayList<>();
        if(this.content != null) {
            contenttmp.addAll(this.content);
        }
        return contenttmp;
    }

    /**
     * @param content the content to set
     */
    public void setContent(List<FacturaBean> content) {
        List<FacturaBean> contenttmp = new ArrayList<>();
        if(content != null) {
            contenttmp.addAll(content);
        }
        this.content = contenttmp;
    }
}
